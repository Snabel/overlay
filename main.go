package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"sync"

	"github.com/gempir/go-twitch-irc"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

var app *Overlay
var file *os.File
var client []byte
var settings []byte

func init() {
	app = &Overlay{listeners: []func(interface{}) error{}, state: State{}}
	f, err := os.OpenFile("state", os.O_CREATE|os.O_RDWR, 0644)
	if err != nil {
		log.Fatalln("open state error", err)
	}
	statejs, err := ioutil.ReadAll(f)
	if err != nil {
		log.Fatalln("read state error", err)
	}
	if len(statejs) > 0 {
		if err := json.Unmarshal(statejs, &app.state); err != nil {
			log.Println("parse state error, resetting", err)
		} else {
			log.Println("state loaded", app.state)
		}
	}
	file = f
	client, err = ioutil.ReadFile("client.html")
	if err != nil {
		log.Fatalln("open client file error", err)
	}
	settings, err = ioutil.ReadFile("settings.html")
	if err != nil {
		log.Fatalln("open settings file error", err)
	}
}

func main() {
	// gin.SetMode(gin.ReleaseMode)
	router := gin.Default()
	// router.Use(gin.Recovery())
	router.GET("/overlay", func(c *gin.Context) {
		overlayAccept(c.Writer, c.Request)
	})
	router.GET("/", func(c *gin.Context) {
		c.Data(200, "text/html", client)
	})
	router.GET("/settings", func(c *gin.Context) {
		c.Data(200, "text/html", settings)
	})
	tw := twitch.NewAnonymousClient()
	tw.SendPings = true
	tw.Join("birdietv")
	tw.OnPrivateMessage(func(msg twitch.PrivateMessage) {
		if len(msg.Emotes) > 0 {
			for _, e := range msg.Emotes {
				app.broadcast(map[string]interface{}{"twitch_emote": e})
			}
		}
	})
	go tw.Connect()
	router.Run(":6321")
}

var overlayWSUpgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin:     func(r *http.Request) bool { return true },
}

func overlayAccept(w http.ResponseWriter, r *http.Request) {
	conn, err := overlayWSUpgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Println("Failed to set websocket upgrade: %+v", err)
		return
	}
	go overlayClient(conn)
}

func overlayClient(conn *websocket.Conn) {
	app.listenerAdd(conn.WriteJSON)
	log.Println("connect", conn.RemoteAddr())
	defer conn.Close()
	defer log.Println("disconnect", conn.RemoteAddr(), app.listeners)
	for {
		_, msg, err := conn.ReadMessage()
		if err != nil {
			log.Println("read error", conn.RemoteAddr(), err)
			return
		}
		v := Update{}
		if err := json.Unmarshal(msg, &v); err != nil {
			log.Println("unmarshal error", conn.RemoteAddr(), err)
			return
		}
		log.Println("update", conn.RemoteAddr(), v)
		app.update(v)
	}
}

type Overlay struct {
	lock      sync.RWMutex
	listeners []func(interface{}) error
	state     State
}

type State struct {
	Ticker       string
	TwitchEmotes bool
	Calendar     bool
	Watermark    bool
}

type Update struct {
	TickerSet    *string
	CardShow     *Card
	TwitchEmotes *bool
	Calendar     *bool
	Watermark    *bool
}

type Card struct {
	Title    string
	Subtitle string
	Position string
}

func (o *Overlay) listenerAdd(fn func(interface{}) error) {
	o.lock.Lock()
	defer o.lock.Unlock()
	if err := fn(map[string]State{"state": o.state}); err != nil {
		return
	}
	o.listeners = append(o.listeners, fn)
}

func (o *Overlay) broadcast(msg interface{}) {
	valid := []func(interface{}) error{}
	for _, fn := range o.listeners {
		if err := fn(msg); err == nil {
			valid = append(valid, fn)
		}
	}
	o.listeners = valid
}

func (o *Overlay) update(update Update) {
	o.lock.Lock()
	defer o.lock.Unlock()
	// update state
	if update.TickerSet != nil {
		o.state.Ticker = *update.TickerSet
	}
	if update.TwitchEmotes != nil {
		o.state.TwitchEmotes = *update.TwitchEmotes
	}
	if update.Calendar != nil {
		o.state.Calendar = *update.Calendar
	}
	if update.Watermark != nil {
		o.state.Watermark = *update.Watermark
	}
	// end
	o.broadcast(map[string]interface{}{"update": update, "state": o.state})
	statejs, err := json.Marshal(o.state)
	if err != nil {
		log.Println("state save error", err)
	} else {
		file.Seek(0, 0)
		file.Truncate(0)
		if _, err := file.Write(statejs); err != nil {
			log.Println("state write error", err)
		}
	}
}
